# Introduction

## SHERPA

[Sherpa](https://sherpa-team.gitlab.io/) is a Monte Carlo event generator for the **S**imulation of **H**igh-**E**nergy **R**eactions of **PA**rticles in lepton-lepton, lepton-photon, photon-photon, lepton-hadron and hadron-hadron collisions. Simulation programs - also dubbed event generators - like Sherpa are indispensable work horses for current particle physics phenomenology and are (at) the interface between theory and experiment.

## About this tutorial

This tutorial will introduce the [Sherpa standalone](./Hands-on/Standalone/README.md#hands-on-standalone) and [CMSSW Sherpa interface](./Hands-on/CMS_usage/README.md#sherpa-hands-on-cms-usage) in two seperate parts. We take the Drell-Yan process as an example to introduce the basic configuration settings in Sherpa Run.dat, and run Sherpa generator to generate events, which are then analyzed with the Rivet interface.

## Pre-requisites

- Have the access to `lxplus`. We will use `lxplus9` for this tutorial.
- Better to [set the container](./Hands-on/Standalone/README.md#download-container-image) before the tutorial for standalone session. If you encounter any problems with setting the container, please try to set the `Sherpa` executor from **CMSSW** as discussed in the same [section](https://cms-sherpa-tutorial-2024.docs.cern.ch/Hands-on/Standalone/).
- During the hands-on sessions, we will need to look at the diagrams, histograms, ... Therefore, if you already know how to view these produced on the lxplus, that would be great.

    !!! tip

        -  Using `ssh -XY` to enable `X11` forwarding for GUI, if you have a stable connection to lxplus. Then you can use firefox to open html files that organize the plots as a webpage.

        -  To browser plots and files interactively, we could follow the suggestion from common analysis tool group: [Interactive Plot Browser](https://cms-analysis.docs.cern.ch/guidelines/other/plot_browser/#manage-access-control).

            In this way, we could put plots to the **EOS**. Then the plots could be viewed from your own website with the [plot browser](https://cms-analysis.docs.cern.ch/guidelines/other/plot_browser/#install-the-plot-browser).

        - Simply download the folder of the plots locally.