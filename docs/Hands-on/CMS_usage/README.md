# Sherpa Hands-on: CMS usage <a id="cms_chap2"></a>

This tutorial follows the previous CMS Sherpa tutorials and use the Drell-Yan Z+Jets process as an example.

!!! info

    Reference:

    1. CMS Sherpa Tutorial 2019 [link](https://twiki.cern.ch/twiki/bin/viewauth/CMS/SherpaTutorial2019)

    2. CMS Sherpa Tutorial Exercise2 [link](https://twiki.cern.ch/twiki/bin/view/CMS/SherpaTutorialExercise2)

    3. Using Sherpa in CMSSW [link](https://indico.cern.ch/event/779232/contributions/3242522/attachments/1788945/2914410/Sherpa_cmssw_Jan_2019.pdf)

## CMSSW Sherpa Interface

The purpose of the Sherpa Interface is to allow event generation within the [**CMSSW**](https://cms-sw.github.io/) framework. Generating events with Sherpa is divided into three main parts:

1. Initialization: creation of feynman diagrams / helicity amplitudes / libraries
2. Phase-space integration / cross-section calculation
3. Actual event generation

The Sherpa Interface is responsible for the last step starting from a CMSSW python fragment and a **Sherpack**, which contains the results from the previous steps. In addition to the interface, a set of scripts is provided in `GeneratorInterface/SherpaInterface/data` to take care of the preceding steps and the Sherpack creation.

- `MakeSherpaLibs.sh` executes step 1 & 2 starting from a Sherpa Run.dat

- `PrepareSherpaLibs.sh` creates a Sherpack from the `MakeSherpaLibs.sh` results as well as a python configuration file

Additional information on how to use the scripts and the interface will be given in [**Step by Step Exercise**](#cms1). More details on the different steps and methods of Sherpa can be found in the [Sherpa manual](https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.15.html).

## Step by Step Exercise <a id="cms1"></a>

### Goal of the tutorial

1. Setting up `SherpaInterface`
2. Familiarise on how to create **Sherpack** and generate event
3. Able to generate process at LO/NLO and compares the two samples (using [Rivet](https://rivet.hepforge.org/) plotting facility)

### Setting up environment

After logging into **lxplus9**. The first step is to set up your CMSSW environment with the following commands

```bash title="⏳ 20s"
mkdir cmssw_sherpa
cd cmssw_sherpa

scram project CMSSW_13_2_9
cd CMSSW_13_2_9/src
export TOPDIR=$PWD
cmsenv
```

We will be using **CMSSW_13_2_9** with the Sherpa version ([v2.2.15](https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.15.html)). You can check the Sherpa version in your release with the following command

```bash
scram tool info sherpa
```

??? info

    ```bash
    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Name : sherpa
    Version : 2.2.15-3b8bf31f3e9510bc02d046766937faca
    ++++++++++++++++++++

    BINDIR=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/bin
    CMSSW_FWLITE_INCLUDE_PATH=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/include
    INCLUDE=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/include/SHERPA-MC
    LIB=SherpaMain ToolsMath ToolsOrg
    LIBDIR=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/lib/SHERPA-MC
    ROOT_INCLUDE_PATH=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/include/SHERPA-MC
    SHERPA_BASE=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca
    SHERPA_INCLUDE_PATH=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/include/SHERPA-MC
    SHERPA_LIBRARY_PATH=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/lib/SHERPA-MC
    SHERPA_SHARE_PATH=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/share/SHERPA-MC
    USE=root_cxxdefaults hepmc lhapdf qd blackhat fastjet sqlite openmpi openloops
    ```


As a next step, checkout the `SherpaInterface` to setup Sherpa generator
```bash title="⏳ 30s"
git cms-addpkg GeneratorInterface/SherpaInterface
```

Next, we setting up the filesystem skeleton in CMSSW, we will be performing generation and simulation in **test** folder:

```bash
mkdir -p MY/PROJECT/test
mkdir -p MY/PROJECT/python
cd MY/PROJECT/test/
cp $TOPDIR/GeneratorInterface/SherpaInterface/data/*SherpaLibs.sh .
chmod +x *.sh
```

### Create Sherpack

We need a run card for Sherpa to proceed with generation. In this example, we will be using Drell-Yan process: $p p \to Z/\gamma^{*}(\ell \ell) + X$. If we want to generate events at $\sqrt{s} = 13 \mathrm{TeV}$, how do you set the Run.dat? Here are some examples at **LO** and **NLO**:

!!! example

    === "DY @ LO"

        ```ini hl_lines="8 15 16 19-22 26" title="⏳ 6min"
        (run){
            % general setting
            EVENTS 100K; ERROR 0.99;

            % scales, tags for scale variations
            FSF:=1.; RSF:=1.; QSF:=1.;
            SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
            SCALE_VARIATIONS 0.25,0.25 0.25,1. 1.,0.25 1.,1. 1.,4. 4.,1. 4.,4.;

            % me generator settings
            ME_SIGNAL_GENERATOR Comix Amegic;
            EVENT_GENERATION_MODE PartiallyUnweighted;

            % collider setup
            BEAM_1 2212; BEAM_ENERGY_1 = 6500.;
            BEAM_2 2212; BEAM_ENERGY_2 = 6500.;

            # EVENT_OUTPUT=HepMC_GenEvent[MyFile];
            HEPMC_USE_NAMED_WEIGHTS=1;
            PDF_LIBRARY LHAPDFSherpa;
            PDF_SET NNPDF31_nnlo_as_0118_mc_hessian_pdfas;
            PDF_VARIATIONS NNPDF31_nnlo_as_0118_mc_hessian_pdfas[all];
        }(run)

        (processes){
          Process 93 93 -> 90 90;
          Order (*,2);
          End process;
        }(processes)

        (selector){
          Mass 11 -11 50. E_CMS;
          Mass 13 -13 50. E_CMS;
          Mass 15 -15 50. E_CMS;
        }(selector)
        ```

    === "DY + 01Jet @ LO"

        ```ini hl_lines="6 29-30" title="⏳ 20min (9min with 4 threads)"
        (run){
            % general setting
            EVENTS 100K; ERROR 0.99;

            % tags for process setup
            NJET:=1; QCUT:=20.;

            % scales, tags for scale variations
            FSF:=1.; RSF:=1.; QSF:=1.;
            SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
            SCALE_VARIATIONS 0.25,0.25 0.25,1. 1.,0.25 1.,1. 1.,4. 4.,1. 4.,4.;

            % me generator settings
            ME_SIGNAL_GENERATOR Comix Amegic;
            EVENT_GENERATION_MODE PartiallyUnweighted;

            % collider setup
            BEAM_1 2212; BEAM_ENERGY_1 = 6500.;
            BEAM_2 2212; BEAM_ENERGY_2 = 6500.;

            # EVENT_OUTPUT=HepMC_GenEvent[MyFile];
            HEPMC_USE_NAMED_WEIGHTS=1;
            PDF_LIBRARY LHAPDFSherpa;
            PDF_SET NNPDF31_nnlo_as_0118_mc_hessian_pdfas;
            PDF_VARIATIONS NNPDF31_nnlo_as_0118_mc_hessian_pdfas[all];
        }(run)

        (processes){
          Process 93 93 -> 90 90 93{NJET};
          Order (*,2); CKKW sqr(QCUT/E_CMS);
          End process;
        }(processes)

        (selector){
          Mass 11 -11 50. E_CMS;
          Mass 13 -13 50. E_CMS;
          Mass 15 -15 50. E_CMS;
        }(selector)
        ```

    === "DY @ NLO + 01Jet @ LO"

        ```ini title="⏳ 32min with 64 threads"
        (run){
          % general setting
          EVENTS 100K; ERROR 0.99;

          % scales, tags for scale variations
          FSF:=1.; RSF:=1.; QSF:=1.;
          SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
          SCALE_VARIATIONS 0.25,0.25 0.25,1. 1.,0.25 1.,1. 1.,4. 4.,1. 4.,4.;

          % tags for process setup
          NJET:=1; LJET:=2; QCUT:=20.;

          % me generator settings
          ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
          EVENT_GENERATION_MODE PartiallyUnweighted;
          OL_PARAMETERS ew_renorm_scheme 1;
          LOOPGEN:=OpenLoops;
          PP_RS_SCALE VAR{0.25*H_Tp2};

          % collider setup
          BEAM_1 2212; BEAM_ENERGY_1 = 6500.;
          BEAM_2 2212; BEAM_ENERGY_2 = 6500.;

          # EVENT_OUTPUT=HepMC_GenEvent[MyFile];
          HEPMC_USE_NAMED_WEIGHTS=1;
          PDF_LIBRARY LHAPDFSherpa;
          PDF_SET NNPDF31_nnlo_as_0118_mc_hessian_pdfas;
          PDF_VARIATIONS NNPDF31_nnlo_as_0118_mc_hessian_pdfas[all];
        }(run)

        (processes){
          Process 93 93 -> 90 90 93{NJET};
          Order (*,2); CKKW sqr(QCUT/E_CMS);
          NLO_QCD_Mode MC@NLO {LJET};
          ME_Generator Amegic {LJET};
          RS_ME_Generator Comix {LJET};
          Loop_Generator LOOPGEN {LJET};
          Integration_Error 0.02 {2};
          Integration_Error 0.02 {3};
          End process;
        }(processes)

        (selector){
          Mass 11 -11 50. E_CMS;
          Mass 13 -13 50. E_CMS;
          Mass 15 -15 50. E_CMS;
        }(selector)
        ```

    === "DY + 012Jet @ NLO + 34Jet @ LO"

        ```ini
        (run){
          % general setting
          EVENTS 100K; ERROR 0.99;

          % scales, tags for scale variations
          FSF:=1.; RSF:=1.; QSF:=1.;
          SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
          SCALE_VARIATIONS 0.25,0.25 0.25,1. 1.,0.25 1.,1. 1.,4. 4.,1. 4.,4.;
          ASSOCIATED_CONTRIBUTIONS_VARIATIONS EW EW|LO1;

          % tags for process setup
          NJET:=4; LJET:=2,3,4; QCUT:=20.;

          % me generator settings
          ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
          EVENT_GENERATION_MODE PartiallyUnweighted;
          OL_PARAMETERS ew_renorm_scheme 1;
          LOOPGEN:=OpenLoops;
          PP_RS_SCALE VAR{0.25*H_Tp2};

          % collider setup
          BEAM_1 2212; BEAM_ENERGY_1 = 6500.;
          BEAM_2 2212; BEAM_ENERGY_2 = 6500.;

          # EVENT_OUTPUT=HepMC_GenEvent[MyFile];
          HEPMC_USE_NAMED_WEIGHTS=1;
          PDF_LIBRARY LHAPDFSherpa;
          PDF_SET NNPDF31_nnlo_as_0118_mc_hessian_pdfas;
          PDF_VARIATIONS NNPDF31_nnlo_as_0118_mc_hessian_pdfas[all];
        }(run)

        (processes){
          Process 93 93 -> 90 90 93{NJET};
          Order (*,2); CKKW sqr(QCUT/E_CMS);
          NLO_QCD_Mode MC@NLO {LJET};
          ME_Generator Amegic {LJET};
          RS_ME_Generator Comix {LJET};
          Loop_Generator LOOPGEN {LJET};
          Associated_Contributions EW|LO1 {LJET};
          Enhance_Observable VAR{log10(PPerp(p[2]+p[3]))}|1.|3. {3,4,5,6};
          Integration_Error 0.02 {2};
          Integration_Error 0.02 {3};
          Integration_Error 0.02 {4};
          Integration_Error 0.05 {5};
          Integration_Error 0.05 {6};
          End process;
        }(processes)

        (selector){
          Mass 11 -11 50. E_CMS;
          Mass 13 -13 50. E_CMS;
          Mass 15 -15 50. E_CMS;
        }(selector)
        ```

<!-- You may take a look at Run.dat_DY_01Jet_LO content to have an idea of how sherpa run card is defined. For more information you may find the definition of each parameters in [Sherpa manual](https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.15.html).  -->
We are ready to perform initialization and phase space integration to generate Sherpack. Let us start with Run.dat_DY_LO, invoke <a id=cms2></a>

```bash title="⏳ 6min"
cp /afs/cern.ch/work/j/jixiao/public/sherpa_tutorial/cmssw/Run.dat_DY_LO .
./MakeSherpaLibs.sh -p DY_LO -o LBCR -v
```

You may have a look at the input parameters of the MakeSherpaLibs.sh script. Expanding the human readable menu by

```bash
./MakeSherpaLibs.sh -h
```

??? info

    ```bash
    MakeSherpaLibs version 4.4

    options: -d  path       (optional) path to your SHERPA installation (otherwise the SHERPA
                            package belonging to the release under '$CMSSW_BASE' is used)
                            -> ( /cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca )
            -i  path       path to SHERPA datacard (and library, see -o) files
                            -> ( /afs/cern.ch/work/j/jixiao/sherpa_tutorial/cmssw_sherpa/CMSSW_13_2_9/src/MY/PROJECT/test )
            -p  process    SHERPA process/dataset name ( XXX )
            -o  option     library/cross section options ( LIBS )
                            [ 'LBCR' : generate libraries and cross sections     ]
                            [ 'LIBS' : generate libraries only                   ]
                            [ 'CRSS' : generate cross sections, needs libraries! ]
                            [ 'EVTS' : generate events, needs libs + crss. sec.! ]
            -f  path       output path for SHERPA library & cross section files
                            -> ( /afs/cern.ch/work/j/jixiao/sherpa_tutorial/cmssw_sherpa/CMSSW_13_2_9/src/MY/PROJECT/test )
            -D  filename   (optional) name of data card file (  )
            -L  filename   (optional) name of library file (  )
            -C  filename   (optional) name of cross section file (  )
            -A             switch on multiple interactions in Run.dat card ( FALSE )
            -v             verbose mode ( FALSE )
            -T             disable library compilation in multithreading mode ( FALSE )
            -m  command    enable running in MPI mode with command (  )
            -M  option     additional options for MPI command: (  )
            -e  # evts.    number of events to be produced ( 0 )
            -h             display this help and exit
    ```

    **MPI parallelization** in Sherpa can be enabled. Sherpa supports [OpenMPI](http://www.open-mpi.org/) and [MPICH2](http://www.mcs.anl.gov/research/projects/mpich2/). To speed up the phase-space integration, we could take advantage of sherpa's support for MPI by adding, e.g., `-m mpirun -M '-n 9'` to [`MakeSherpaLibs.sh` script](#cms2)


The phase-space integration is very time consuming for complicated processes, higher orders and high jet multiplicities (hours/ days/ weeks ...). Generating Drell-Yan process with additional 1 jets is fairly fine.

Once the script has finished you should find the following new files in you directory:

```bash
$ cd ..
$ tree test -P sherpa_DY_LO*.tgz
test
|-- sherpa_DY_LO_crdE.tgz
|-- sherpa_DY_LO_crss.tgz
|-- sherpa_DY_LO_libs.tgz
|-- sherpa_DY_LO_logL.tgz
`-- sherpa_DY_LO_migr.tgz
$ cd -
```

These archives contain

- `*_libs.tgz`: the Sherpa libraries
- `*_crss.tgz`: the corresponding cross sections
- `*_logL.tgz`: the log files
- `*_crdE.tgz`: the cards which are needed for event generation
- `*_migr.tgz`: a phase space grid for multiple interactions (**MPI**)

The latter file is optional, everything should also work without it. Now, you have all the ingredients to create a Sherpack, invoke

```bash
./PrepareSherpaLibs.sh -p DY_LO
```

??? info

    ```bash
    <I>  generated sherpack:
    <I>    sherpa_DY_LO_MASTER.tgz
    <I>  generated python fragment:
    <I>    sherpa_DY_LO_MASTER_cff.py
    <I>  MD5 checksum file:
    <I>    sherpa_DY_LO_MASTER.md5
    <I>
    <I>
    <I>
    <I> ATTENTION,
    <I>  please edit the generated python fragment according to your needs:
    <I>
    <I>  for a local production edit the line
    <I>    SherpackLocation = cms.string('[local path to your sherpack]')
    <I>  and make sure that the SherpaInterface does not try to fetch the sherpack
    <I>    FetchSherpack = cms.bool(False)
    <I>
    <I>
    <I>  for a production using the frontier database use
    <I>    SherpackLocation = cms.string('[frontier database path to your sherpack]')
    <I>  and this time make sure that the SherpaInterface fetches the sherpack
    <I>    FetchSherpack = cms.bool(true)
    <I>
    <I>
    <I>  for a production with CRAB add the sherpack to the list of additional files
    <I>    additional_input_files = [name of the ..._MASTER.tgz sherpack]
    <I>  make sure that the sherpack location is
    <I>    SherpackLocation = cms.string('./')
    <I>  and make sure that the SherpaInterface does not try to fetch the sherpack
    <I>    FetchSherpack = cms.bool(False)
    <I>
    <I>
    <I>
    <I>  a good way to test the generated python file is to cross-check it with cmsDriver.py:
          cmsDriver.py A/B/python/sherpa_DY_LO_MASTER_cff.py \
            -s GEN -n 100 --no_exec --conditions auto:mc --eventcontent RAWSIM
    <I>
    <I>
    <I>
    ```

The script should have created a Sherpack, a file containing the md5 checksum of the Sherpack and a python configuration fragment as shown below. If you open the python fragment with an editor, you will see that it contains the configuration for `SherpaHadronizerFilter` with information about the Sherpack and the runcard:

```bash
$ cd ..
$ tree test -P sherpa*DY_LO*MASTER*
test
|-- sherpa_DY_LO_MASTER.md5
|-- sherpa_DY_LO_MASTER.tgz
`-- sherpa_DY_LO_MASTER_cff.py
$ cd -
```

### Event Generation

Event generation is proceeded by using CMSSW applications, `cmsdriver.py` and `cmsRun`. The first step is to copy the python `fragment` into the python directory of your working area and **compile**

```bash title="⏳ 20s"
cp sherpa_DY_LO_MASTER_cff.py ../python
cd ..
scram b
cd test
```

Next, we will use `cmsDriver.py` to generate python configuration file, which later will be used for `cmsRun` command:

```bash
cmsDriver.py MY/PROJECT/python/sherpa_DY_LO_MASTER_cff.py -s GEN -n 10000 --no_exec --conditions auto:mc --eventcontent RAWSIM
```

Then let's run the python configuration file to generate events:

```bash title="⏳ 5min"
cmsRun sherpa_DY_LO_MASTER_cff_py_GEN.py
```

!!! question

    1. How many events we have requested?
    2. What's the total cross section of this process?
    3. In which file are the output events stored? What is the size of this file?

??? tip

    - `GenXsecAnalyzer` module in CMSSW will give some values:

      ```bash
      ------------------------------------
      GenXsecAnalyzer:
      ------------------------------------
      Before Filter: total cross section = 5.655e+03 +- 4.841e+01 pb
      Filter efficiency (taking into account weights)= (2.11589e+08) / (2.11589e+08) = 1.000e+00 +- 0.000e+00
      Filter efficiency (event-level)= (10000) / (10000) = 1.000e+00 +- 0.000e+00    [TO BE USED IN MCM]

      After filter: final cross section = 5.655e+03 +- 4.841e+01 pb
      After filter: final fraction of events with negative weights = 0.000e+00 +- 0.000e+00
      After filter: final equivalent lumi for 1M events (1/fb) = 1.768e-01 +- 1.524e-03
      ```

    - The output events are store in a Root file with the [CMS EDM dataformat](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookAnalysisOverviewIntroduction). Thanks to **Nano** datatier, we could get a flat Root tree:

      ```bash title="⏳ 30s"
      cmsDriver.py MY/PROJECT/python/sherpa_DY_LO_MASTER_cff.py -s GEN,NANOGEN -n 100 --no_exec --conditions auto:mc --datatier NANOAODSIM --eventcontent NANOAODSIM
      cmsRun sherpa_DY_LO_MASTER_cff_py_GEN_NANOGEN.py
      ```

      Then you can explore the output Root file `sherpa_DY_LO_MASTER_cff_py_GEN_NANOGEN.root`.


We will analyse the events with **Rivet**. By interfacing with Rivet, CMSSW allows us pass the events to Rivet directly using the following python fragment:

```python
import FWCore.ParameterSet.Config as cms

def customise(process):
    process.load("GeneratorInterface.RivetInterface.rivetAnalyzer_cfi")

    process.rivetAnalyzer.AnalysisNames = cms.vstring("CMS_2018_I1667854", "CMS_2019_I1753680", "MC_ZINC","MC_ZJETS","MC_XS","MC_GENERIC")
    process.rivetAnalyzer.OutputFile = cms.string('rivet_analysis.yoda')
    # CrossSection should either be a NNLO value, or -1 for the generator xsec
    process.rivetAnalyzer.CrossSection = cms.double(-1)
    # process.rivetAnalyzer.setNominalWeightName="MUR1_MUF1_PDF325300"
    process.rivetAnalyzer.selectMultiWeights = cms.string('.*MUR.*MUF.*PDF325300')

    # process.generation_step+=process.genParticles2HepMC
    process.generation_step+=process.rivetAnalyzer
    return(process)
```

To integrate this python fragment and run the Rivet analyses, run:

```bash title="⏳ 6min"
cp /afs/cern.ch/work/j/jixiao/public/sherpa_tutorial/cmssw/rivet_customize_13TeV_DY.py ../python/
cmsDriver.py MY/PROJECT/python/sherpa_DY_LO_MASTER_cff.py -s GEN -n 10000 --no_exec --conditions auto:mc --eventcontent RAWSIM --customise=MY/PROJECT/rivet_customize_13TeV_DY.py
cmsRun sherpa_DY_LO_MASTER_cff_py_GEN.py
```

### Plotting Results

Now let us check what plots we've got from the Rivet analyses:

```bash title="⏳ 1min"
# RENAME OUTPUT FILE TO AVOID YODA OVERWRITTEN !!
mv rivet_analysis.yoda DY_LO.yoda
rivet-mkhtml --mc-errs DY_LO.yoda:Title="DY @ LO" -o rivet-plots_DY_LO
```

!!! question

    - How is the agreement between the MC and Data?
    - Do you understand the difference between data and MC in the "differential cross section of Z boson production in association with jets"?

??? tip

    If you successfully generate the plots, you should get plots same as this [link](https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/cmssw/rivet-plots_DY_LO/). Check the Run.dat of your process, do you think it can describe events with additional jets very well?

## Generate Sherpack in One Go

A preliminary script: [sherpack_generation.sh](https://github.com/cms-sw/genproductions/blob/master/bin/Sherpa/sherpack_generation.sh) is offered to generate Sherpack, which is available in [genproductions repository](https://github.com/cms-sw/genproductions/tree/master/bin/Sherpa). Let us try this script to generate events with `Run.dat_DY_01Jet_LO`.

### Download **genproductions**

The repository [genproductions](https://github.com/cms-sw/genproductions/tree/master) collects the generator fragments for MC production in CMS experiment. The package includes the datacards used for various generators inclusing POWHEG, MG5_aMC@NLO, Sherpa, Phantom, Pythia ...

```bash title="⏳ 1min"
cd ../../../../../
git clone --depth 1 https://github.com/cms-sw/genproductions.git
```

### Generate the Sherpack

This script `sherpack_generation.sh` must be run in a clean environment as it sets up CMSSW itself. So please try from a clean shell. To generate the Sherpack, just type:

```bash title="⏳ 22min"
cd genproductions/bin/Sherpa
cp /afs/cern.ch/work/j/jixiao/public/sherpa_tutorial/cmssw/Run.dat_DY_01Jet_LO .
./sherpack_generation.sh DY_1Jet_LO Run.dat_DY_01Jet_LO 4
```

??? tip

    In this test, the arguments are:

    ```yml
    Process name: DY_1Jet_LO
    Input Run.dat: Run.dat_DY_01Jet_LO
    Number of runing cores: 4
    ```

    The files of the Sherpack are placed into the folder `DY_1Jet_LO_sherpack`:

    ```bash
    $ tree DY_1Jet_LO_sherpack
    DY_1Jet_LO_sherpack/
    ├── Run.dat_DY_01Jet_LO
    ├── sherpa_DY_01Jet_LO_MASTER_cff.py
    ├── sherpa_DY_01Jet_LO_MASTER.md5
    └── sherpa_DY_01Jet_LO_MASTER.tgz
    ```

### Generate Events and make plots

To generate events and make plots. We can copy the Sherpack files to a proper folder, e.g., the same subfolder of CMSSW_13_2_9 in section [**Step by Step Exercise**](#cms1), then generate events and obtain the Yoda file:

```bash title="⏳ 18min"
cp DY_1Jet_LO_sherpack/* ../../../CMSSW_13_2_9/src/MY/PROJECT/test/
cd ../../../CMSSW_13_2_9/src/MY/PROJECT/test/
cmsenv

# put python fragment into python
cp sherpa_DY_1Jet_LO_MASTER_cff.py ../python
# compile
cd ../python
scram b
# generate configuration of event generation
cmsDriver.py MY/PROJECT/python/sherpa_DY_1Jet_LO_MASTER_cff.py -s GEN -n 10000 --no_exec --conditions auto:mc --eventcontent RAWSIM --customise=MY/PROJECT/rivet_customize_13TeV_DY.py
# generate events and make plots for Rivet analyses
cmsRun sherpa_DY_1Jet_LO_MASTER_cff_py_GEN.py
mv rivet_analysis.yoda DY_01Jet_LO.yoda
```

Let us compare the `DY_01Jet_LO.yoda` with the `DY_LO.yoda`:

```bash title="⏳ 6min"
rivet-mkhtml --mc-errs DY_LO.yoda:Title="DY @ LO" DY_01Jet_LO.yoda:Title="DY+01Jet @ LO" -o rivet-plots_DY_LO_with_Jet
```

!!! question

    Which sample do you think is better? Why?

## Shortcut

We have a limited time on the hands-on session. If the Sherpack generation or event generation take a lot of time, and you don't manage to make the plots. You could copy Sherpacks or Yoda files from here to make plots:

??? danger

    The Sherpacks are available in folder `/afs/cern.ch/work/j/jixiao/public/sherpa_tutorial/cmssw/sherpack/`:

    ```bash
    sherpack/
    ├── DY_01Jet_LO_sherpack
    │   ├── Run.dat_DY_01Jet_LO
    │   ├── sherpa_DY_01Jet_LO_MASTER_cff.py
    │   ├── sherpa_DY_NLO_01Jet_LO_MASTER.md5
    │   └── sherpa_DY_NLO_01Jet_LO_MASTER.tgz
    ├── DY_LO_sherpack
    │   ├── Run.dat_DY_LO
    │   ├── sherpa_DY_LO_MASTER_cff.py
    │   ├── sherpa_DY_LO_MASTER.md5
    │   └── sherpa_DY_LO_MASTER.tgz
    └── DY_NLO_01Jet_LO_sherpack
        ├── Run.dat_DY_NLO_01Jet_LO
        ├── sherpa_DY_NLO_01Jet_LO_MASTER_cff.py
        ├── sherpa_DY_NLO_01Jet_LO_MASTER.md5
        └── sherpa_DY_NLO_01Jet_LO_MASTER.tgz
    ```

    The Yoda files are available in folder `/afs/cern.ch/work/j/jixiao/public/sherpa_tutorial/cmssw/yodafile/`:

    ```bash
    yodafile/
    ├── DY_01Jet_LO.yoda
    ├── DY_LO.yoda
    └── DY_NLO_01Jet_LO.yoda
    ```

!!! question

    I guess you've noticed that the yoda file for **NLO** is there `DY_NLO_01Jet_LO.yoda`. Try to compare this with the LO samples. What do you find in the comparison?

??? tip

    You could also check the comparison plots between LO and NLO [here](https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/cmssw/rivet-plots_DY_LO_NLO/).

## Event Weights, Scale- and PDF-Variations (Optional)

In Sherpa it is possible to calculate alternative event weights for different PDF or scale choices. This can be done on-the-fly and results in additional weights for each generated events. The variations have to be specified in the run card by adding the following lines to the run section of your runcard

```ini
  HEPMC_USE_NAMED_WEIGHTS=1;
  SCALE_VARIATIONS 0.25,0.25 0.25,1. 0.25,4 1,0.25 1.,1. 1.,4. 4.,0.25 4.,1. 4.,4.;
  PDF_LIBRARY LHAPDFSherpa;
  PDF_SET NNPDF31_nnlo_as_0118_mc_hessian_pdfas;
  PDF_VARIATIONS NNPDF31_nnlo_as_0118_mc_hessian_pdfas[all];
```

The first line will allow the additional weights to be stored in the event. The second line specifies the scale variations. Each x,y block represents a variation of the renormalization scale muR and the factorization scale muF. Scales in Sherpa are given in quadratic form. A variation of the **squared scale** by the factor of 4 will result in a variation of the scale itself by a factor of 2. The 3rd - 5th line specifies the PDF variations.

If you want to find out which weights Sherpa will calculate you can run Sherpa standalone with your runcard as shown below:

```bash
PATH/TO/EXECUTABLE/Sherpa -f Run.dat_DY_NLO_01Jet_LO
```

The path to the Sherpa executable is specified as BINDIR when executing scram tool info sherpa. The weights will be displayed before the integration of the actual process. For the variations given above the weights are named as follows.


### Produce Events with Scale- and PDF-Variations

To save time, we will only consider the QCD Scale Variations in this section. To store the information about the weight names and order in CMSSW you need to pass the information to the Sherpa Interface. This can be done by passing a python like the one in `GeneratorInterface/SherpaInterface/python/ExtendedSherpaWeights_cfi.py` via the `-e` option of `PrepareSherpaLibs.sh` when creating the Sherpack. This python file need to contain two lists as shown below. The first list **SherpaWeights** contains the standard weights which are present in Sherpa for each event. The second list **SherpaVariationWeights** contains all the weights from the specified variations. The weights will be stored in the CMSSW event record in the order you specify in this file. Insdead of storing all weights, we only consider QCD Scale variations:

```python
import FWCore.ParameterSet.Config as cms

SherpaWeightsBlock = cms.PSet(
    SherpaWeights = cms.vstring(
                  'Weight',
                  'MEWeight',
                  'WeightNormalisation',
                  'NTrials'
    ),
    SherpaVariationWeights = cms.vstring(
                  'MUR0.5_MUF0.5_PDF325300',
                  'MUR0.5_MUF1_PDF325300',
                  'MUR1_MUF0.5_PDF325300',
                  'MUR1_MUF1_PDF325300',
                  'MUR1_MUF2_PDF325300',
                  'MUR2_MUF1_PDF325300',
                  'MUR2_MUF2_PDF325300'
    )
)
```

As a first step to produce events with additional weights, you need to prepare a python file with the weights corresponding to the variations in the Run.dat:

```bash
cd $TOPDIR/MY/PROJECT/test
cp /afs/cern.ch/work/j/jixiao/public/sherpa_tutorial/cmssw/QCDScaleSherpaWeights_cfi.py $TOPDIR/GeneratorInterface/SherpaInterface/python
```

Now, create a new sherpack including the weights file:

```bash
./PrepareSherpaLibs.sh -i ./ -p DY_NLO_01Jet_LO -e GeneratorInterface/SherpaInterface/python/QCDScaleSherpaWeights_cfi.py
```

You can then produce events as before using similar cmsDriver and cmsRun commands as in former exercise:

```bash
cmsDriver.py MY/PROJECT/python/sherpa_DY_NLO_01Jet_LO_MASTER_cff.py -s GEN -n 10000 --no_exec --conditions auto:mc --eventcontent RAWSIM --customise=MY/PROJECT/rivet_customize_13TeV_DY.py

cmsRun sherpa_DY_NLO_01Jet_LO_MASTER_cff_py_GEN.py
mv rivet_analysis.yoda DY_NLO_01Jet_LO_QCDScaleWgt.yoda
```

### Analyze Events with Scale- and PDF-Variations

We are still using the Rivet analyses, so let us directly make plots with the output Yoda file:

```bash
rivet-mkhtml --mc-errs DY_NLO_01Jet_LO_QCDScaleWgt.yoda:'BandComponentEnv=MUR.*_MUF.*PDF325300:Variations=none' -o rivet-plots_QCDScale --num-threads 8
```

??? tip

    The plots with QCD Scale uncertainty is available [here](https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/cmssw/new/rivet-plots_QCDScale/).