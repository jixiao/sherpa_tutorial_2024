# Hands-on: Standalone <a id="cms_chap1"></a>

This tutorial uses the Drell-Yan Z+Jets process as an example.

It is an abridged and modified version of a longer MCnet tutorial given in Durham in 2023:

!!! info

    Reference:

    1. MCnet School 2023 [Sherpa tutorial](https://gitlab.com/hepcedar/mcnet-schools/durham-2023/-/tree/main/sherpa)

## Download container image

We use the container tool [**podman**](https://podman.io/) to run commands in the container of [**rivet-sherpa**](https://hub.docker.com/r/hepstore/rivet-sherpa).

=== "📦 Container"

    Run the following commands to set up and enter the container:

    ```bash title="⏳ 2min"
    podman run -it -v $PWD:$PWD -w $PWD docker://hepstore/rivet-sherpa:3.1.9-2.2.15
    ```

=== "🔀 CMSSW"

    You may encounter some problems when you try to use the container, e.g., limited storage, the container runs very slowly. We can use the built-in Sherpa from CMSSW. The first step is to set up your CMSSW environment with the following commands:

    ```bash title="⏳ 20s"
    mkdir cmssw_sherpa
    cd cmssw_sherpa

    scram project CMSSW_13_2_9
    cd CMSSW_13_2_9/src
    export TOPDIR=$PWD
    cmsenv
    ```

    We will be using **CMSSW_13_2_9** with the Sherpa version ([v2.2.15](https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.15.html)). You can check the Sherpa version in your release with the following command

    ```bash
    scram tool info sherpa
    ```

    ??? info

        ```bash
        ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        Name : sherpa
        Version : 2.2.15-3b8bf31f3e9510bc02d046766937faca
        ++++++++++++++++++++

        BINDIR=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/bin
        CMSSW_FWLITE_INCLUDE_PATH=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/include
        INCLUDE=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/include/SHERPA-MC
        LIB=SherpaMain ToolsMath ToolsOrg
        LIBDIR=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/lib/SHERPA-MC
        ROOT_INCLUDE_PATH=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/include/SHERPA-MC
        SHERPA_BASE=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca
        SHERPA_INCLUDE_PATH=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/include/SHERPA-MC
        SHERPA_LIBRARY_PATH=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/lib/SHERPA-MC
        SHERPA_SHARE_PATH=/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/share/SHERPA-MC
        USE=root_cxxdefaults hepmc lhapdf qd blackhat fastjet sqlite openmpi openloops
        ```

    Now add the Sherpa executor to the PATH:

    ```bash
    export PATH=$PATH:/cvmfs/cms.cern.ch/el9_amd64_gcc11/external/sherpa/2.2.15-3b8bf31f3e9510bc02d046766937faca/bin
    ```

Then we are in the container, check if Sherpa is available:

=== "📦 Container"

    ```bash
    # Now we run commands in the container
    Sherpa --version
    ```

    !!! note

        Since we mainly run commands in the container, it's good to **open a seperate terminal** to help us manipulate files, e.g., copy Run.dat from public folder and move plots to other place, more easily.

=== "🔀 CMSSW"

    ```bash
    # Now we run commands after setting CMSSW
    # You might see some WARNINGs, please ignore them
    Sherpa --version
    ```

## A First Sherpa Run

Many reactions at the LHC suffer from large higher-order QCD corrections. The correct simulation of Bremsstrahlung in these processes is essential. It can be tackled either in the parton-shower approach, or using fixed-order calculations.

Sherpa combines both these methods using a technique known as Matrix Element + Parton Shower merging (ME+PS) [*Phys.Rept.* 504 (2011) 145-233](
https://doi.org/10.1016/j.physrep.2011.03.005).
This tutorial will show you how to use the method in Sherpa.

### The Input File

Sherpa is steered using input files (also called runcards), which consist of several sections,
as discussed in the introduction talk.
A comprehensive list of all input parameters for Sherpa is given in the [Sherpa manual](https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.15.html).

The following input file can be used for a simple Z+jets process simulation:

```Ini
(run){
  % collider setup
  BEAM_1 2212; BEAM_ENERGY_1 = 3500.;
  BEAM_2 2212; BEAM_ENERGY_2 = 3500.;

  % disable non-perturbative simulation stages
  MI_HANDLER Off;
  FRAGMENTATION Off;

  % do not require a full optimisation before event generation
  ERROR 0.99;
  FINISH_OPTIMIZATION Off;

  % disable unweighting
  EVENT_GENERATION_MODE Weighted
}(run);

(processes){
  Process 93 93 -> 11 -11;
  Order (*,2);
  Print_Graphs graphs;
  End process;
}(processes);

(selector){
  Mass 11 -11 66 116
}(selector)
```

Since Sherpa writes out data during its runs, it might be advisable to create separate directories for each run.
Run the following commands to create a directory
and download the above runcard.

=== "📦 Container"

    ```bash
    mkdir drell-yan
    cd drell-yan
    wget https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/standalone/Run.dat
    ```

=== "🔀 CMSSW"

    ```bash
    mkdir drell-yan
    cd drell-yan
    wget https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/standalone/Run.dat
    ```

The simulated reactions are defined
in the `(processes)` section of the runcard.
Particles are identified by their PDG codes,
e.g. 11 stands for an electron, while -11 stands for a positron.
Negative codes denote antiparticles.
To specify the initial state we use the special code 93,
which denotes the jet container.
It stands for any parton, i.e. the light quarks and the gluon.

To regularize the cross section, which would diverge if we included the entire photon contribution,
we use a generator-level cut in the `(selector)` section of the runcard.
It ensures that the invariant mass of the dilepton is close to the Z mass.
In this way, we remove the singular behaviour when the invariant mass becomes zero.

### Running Sherpa

Let's now run Sherpa for the first time,
and check if it indeed generates Drell-Yan events.

Run the following command line:

=== "📦 Container"

    ```bash title="⏳ 10s"
    Sherpa -e0
    ```

=== "🔀 CMSSW"

    ```bash title="⏳ 11s"
    Sherpa -e0
    ```

When run for the first time, Sherpa will produce diagram information for the calculation of hard scattering processes. It will also compute that hard scattering cross sections, which are stored, together with the diagram information, for subsequent runs.

!!! question

    With the option **-e0** we have instructed Sherpa to produce no event. Have a look at the total cross section. Which one do you find?

Now have a look at the Feynman diagrams, which contribute to the simulation of the hard process:

=== "📦 Container"

    ```bash title="⏳ 30s"
    plot_graphs.sh graphs/

    # run firefox in a seperate terminal
    # firefox graphs/index.html
    ```

=== "🔀 CMSSW"

    ```bash
    # failed to generate the diagrams
    # check the graphs in the "tip"
    # plot_graphs.sh graphs/
    # firefox graphs/index.html
    ```

(The **firefox** command should be run on your host machine. Of course you can use any method to open the HTML file on your host machine.)

!!! question

    Are these the diagrams you expect to find?

??? tip

    Click [here](https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/standalone/graphs_dy/) to see the Feynman diagrams, if you could not open the html.

Note that some diagrams that have identical matrix elements will be omitted,
while flipping the initial-state of different incoming partons
gives rise to separate graphs.

Now run the following command line:

=== "📦 Container"

    ```bash title="⏳ 3s"
    Sherpa -e1 -o3
    ```

=== "🔀 CMSSW"

    ```bash title="⏳ 4s"
    Sherpa -e1 -o3
    ```

??? info

    Click [here](https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/standalone/standalone_e1_o3.log) to see the log file.

The option **-e1** used above instructs Sherpa to produce one event, and the option **-o3** will print the result of event generation on the screen. You will see Sherpa's internal event record.

!!! question

    Search for **Signal Process** inside the output and check incoming and outgoing particles. Scroll a bit down, and look for the **Shower** stage. How many partons are emitted by the parton shower?


## ME+PS merging

The current runcard lets Sherpa generate events at lowest order in the strong coupling.
To improve the description of real radiative corrections, we can include higher-multiplicity tree-level contributions in the simulation. This is done by changing the process specification:

```Ini
  Process 93 93 -> 11 -11;
```

to

```Ini
  Process 93 93 -> 11 -11 93{1};
```

The last entry instructs Sherpa to produce up to one additional **jet** using
hard matrix elements and combine the respective process with the leading-order process.
This is known as Matrix Element + Parton Shower merging (ME+PS), or the CKKW method.
The essence of the method is a separation of hard from soft radiative corrections,
achieved using phase-space slicing by means of a variable called the jet criterion.
The slicing parameter is called the merging cut.

Let us assume we want to classify jets of more than 20 GeV transverse momentum as hard.
In Sherpa, the corresponding merging cut would be specified as

```Ini
    CKKW sqr(20/E_CMS);
```

Therefore, the complete `(processes)` section for the merged event sample reads:

```Ini
(processes){
    Process 93 93 -> 11 -11 93{1};
    CKKW sqr(20/E_CMS);
    Order (*,2);
    Print_Graphs graphs;
    End process;
}(processes);
```

Run the new setup.

=== "📦 Container"

    ```bash title="⏳ 1min30s"
    Sherpa -e0
    ```

=== "🔀 CMSSW"

    ```bash title="⏳ 1min20s"
    Sherpa -e0
    ```

!!! note

    Sherpa does not only compute the total cross section for the `j j -> e- e+` process, but also for the `j j -> e- e+ j` process with one additional final-state jet.

If you like, you can have a look at the Feynman graphs again, which contribute
to the ME+PS merged event sample. In this case, you also need to rerun the
plot command from the previous section.

## Analyses

By default, Sherpa does not store events. Run Sherpa with the following command
to write out weighted event files in **HepMC3** format, which can subsequently be analyzed

=== "📦 Container"

    ```bash title="⏳ 1s"
    Sherpa -e 1 EVENT_OUTPUT=HepMC3_GenEvent[events.hepmc]
    ```

=== "🔀 CMSSW"

    ```bash title="⏳ 3s"
    # No HepMC3 support for CMSSW Sherpa yet. So use the HepMC2
     Sherpa -e 1 EVENT_OUTPUT=HepMC_GenEvent[events.hepmc]
    ```

Sherpa will produce a file called `events.hepmc`, which can be analysed with Rivet. Alternatively, you can use Sherpa's internal interface to Rivet, to analyse events directly without writing out large amounts of data first. To do that, add the following line to the `(run)` section of your run
card:

```Ini
  ANALYSIS Rivet
```

To tell Rivet what analysis should be used, you have to add a new section to the
run card:

```Ini
(analysis){
  BEGIN_RIVET {
    -a MC_ZINC;
  } END_RIVET;
}(analysis);
```

The option `-a MC_ZINC` instructs Rivet to run a Monte-Carlo analysis of Drell-Yan events, which will provide us with a few observables that can be used for diagnostic purposes.

You can change the name of the produced yoda file by adding:

```Ini
  ANALYSIS_OUTPUT=Analysis_MEPS
```

in the `(run)` section or alternatively you can specify it via the command line:

=== "📦 Container"

    ```bash title="⏳ 30s"
    Sherpa -e10k -A Analysis_MEPS
    ```

=== "🔀 CMSSW"

    !!! note

        No Rivet support for CMSSW Sherpa. So remove the Rivet part in the Run.dat.

        ??? example

            ```ini
            (run){
              % collider setup
              BEAM_1 2212; BEAM_ENERGY_1 = 3500.;
              BEAM_2 2212; BEAM_ENERGY_2 = 3500.;

              % disable non-perturbative simulation stages
              MI_HANDLER Off;
              FRAGMENTATION Off;

              % do not require a full optimisation before event generation
              ERROR 0.99;
              FINISH_OPTIMIZATION Off;

              % disable unweighting
              EVENT_GENERATION_MODE Weighted
              # ANALYSIS Rivet
            }(run);

            (processes){
              # Process 93 93 -> 11 -11;
              Process 93 93 -> 11 -11 93{1};
              CKKW sqr(20/E_CMS);
              Order (*,2);
              Print_Graphs graphs;
              End process;
            }(processes);

            (selector){
              Mass 11 -11 66 116
            }(selector)
            ```

        We do events generation and Rivet analysis seperately:

        ```bash title="⏳ 1min"
        Sherpa -e 10k EVENT_OUTPUT=HepMC_GenEvent[events.hepmc]
        rivet -a MC_ZINC events.hepmc.hepmc2g -o Analysis_MEPS.yoda
        ```

Here, we have also increase the statistics to 10k events.

You can view the results of this analysis by running the command:

=== "📦 Container"

    ```bash title="⏳ 10s"
    rivet-mkhtml --mc-errs Analysis_MEPS.yoda.gz
    ```

=== "🔀 CMSSW"

    ```bash title="⏳ 22s"
    rivet-mkhtml --mc-errs Analysis_MEPS.yoda
    ```

and opening `rivet-plots/index.html` in a browser.

!!! question

    Now it is your turn: Generate a separate file without ME+PS merging (i.e. using the original set-up of the previous section) and analyze it with Rivet. Compare the results of both runs by using:

    === "📦 Container"

        ```bash
        rivet-mkhtml --mc-errs Analysis_LOPS.yoda.gz:Title=LOPS Analysis_MEPS.yoda.gz:Title=MEPS
        ```

    === "🔀 CMSSW"

        ```bash
        rivet-mkhtml --mc-errs Analysis_LOPS.yoda:Title=LOPS Analysis_MEPS.yoda:Title=MEPS --no-weights
        ```

    Do you observe any differences? Why?

??? tip

    Click [here](https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/standalone/rivet-plots_MEPS/) to see histograms with MEPS after Rivet analysis. Histograms for comparison between MEPS and LOPS are [here](https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/standalone/rivet-plots_MEPS_LOPS/).

## Next steps

If there is still time, feel free to do additional studies.
The sections below suggest a few things to try out, you can go through them in any order.

The first section adds uncertainty estimates to your result, the second enables non-perturbative simulations stages, i.e. Hadronization and Underlying Event, and the last section discusses NLO QCD corrections.

### Uncertainty estimates

Using Sherpa, you can study the renormalization and factorization
scale dependence of the simulation as well as the uncertainty
related to the PDF fit. Thanks to reweighting techniques, this can be done
on-the-fly, without running Sherpa multiple times. The reweighting is enabled in
Sherpa by adding the following flags to the `(run)` section of you input file:

```Ini
  SCALE_VARIATIONS 0.25,0.25 0.25,1 1,0.25 1,4 4,1 4,4;
  PDF_LIBRARY LHAPDFSherpa;
  PDF_SET NNPDF30_nnlo_as_0118;
  PDF_VARIATIONS NNPDF30_nnlo_as_0118[all];
```

The line starting with `SCALE_VARIATIONS` instructs Sherpa to perform a
six-point variation of the renormalization and factorization scales,
with the pairs of numbers representing the scale factors for the squared
renormalization and factorization scales.
The line starting with `PDF_VARIATIONS` instructs Sherpa
to perform a variation using all PDFs in the error set of the `NNPDF30_nnlo_as_0118` PDF set, which
is interfaced through LHAPDF.
Finally, in the middle line the default PDF is set to the nominal PDF of the `NNPDF30_nnlo_as_0118` set.
To make the set available within the container, run the following command:

=== "📦 Container"

    ```bash title="⏳ 2s"
    lhapdf install NNPDF30_nnlo_as_0118
    ```

=== "🔀 CMSSW"

    ```bash
    # all pdfsets of LHAPDF are available in the CMSSW. No need to download
    ```

You will also have to add

```Ini
  HEPMC_USE_NAMED_WEIGHTS 1;
  SKIPWEIGHTS 0;
```

in the `(analysis)` section, after the `BEGIN_RIVET` line.

When you have generated a new sample containing the above variations,
you can include them in the Rivet plot, e.g. by drawing a scale variation band
around the nominal prediction:

=== "📦 Container"

    ```bash title="⏳ 1min"
    # generate new sample
    Sherpa -e10k -A Analysis_MEPS_Theoritic_Unc
    # make plots
    rivet-mkhtml --mc-errs Analysis_MEPS_Theoritic_Unc.yoda.gz:'BandComponentEnv=MUR.*_MUF.*_PDF261000:Variations=none' -o rivet-plots_MEPS_Theoritic_Unc
    ```

=== "🔀 CMSSW"

    ```bash title="⏳ 2min"
    # generate new sample
    Sherpa -e 10k EVENT_OUTPUT=HepMC_GenEvent[events_MEPS_Theoritic_Unc.hepmc]
    rivet -a MC_ZINC events_MEPS_Theoritic_Unc.hepmc.hepmc2g -o Analysis_MEPS_Theoritic_Unc.yoda

    # make plots
    rivet-mkhtml --mc-errs Analysis_MEPS_Theoritic_Unc.yoda:'BandComponentEnv=MUR.*_MUF.*_PDF261000:Variations=none' -o rivet-plots_MEPS_Theoritic_Unc
    ```

??? tip

    [Here](https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/standalone/rivet-plots_MEPS_Theoritic_Unc/) shows the plots with QCD Scale uncertainty for DY+01Jet MEPS

With 10k events, the statistical error (given by the vertical bars) should begin to be a bit smaller than the scale variation (given by the band).

### Hadronization and underlying event

So far, multiple parton interactions, which provide a model for the underlying event,
have not been simulated. Also, hadronization has not been included in order to speed up
event generation. You can enable both by removing or commenting the lines:

```Ini
  MI_HANDLER Off;
  FRAGMENTATION Off;
```

!!! question

    How do the predictions for the observables change and why?

??? tip

    === "📦 Container"

        ```bash title="⏳ 6min30s"
        # generate new sample
        Sherpa -e10k -A Analysis_MEPS_UE
        # make plots
        rivet-mkhtml --mc-errs Analysis_MEPS.yoda.gz:Title=MEPS Analysis_MEPS_UE.yoda.gz:Title=MEPS_UE
        ```

    === "🔀 CMSSW"

        !!! note

            Again, no Rivet support for CMSSW Sherpa. So remove the Rivet part in the Run.dat.

            ??? example

                ```ini
                (run){
                % collider setup
                BEAM_1 2212; BEAM_ENERGY_1 = 3500.;
                BEAM_2 2212; BEAM_ENERGY_2 = 3500.;

                % disable non-perturbative simulation stages
                # MI_HANDLER Off;
                # FRAGMENTATION Off;

                % do not require a full optimisation before event generation
                ERROR 0.99;
                FINISH_OPTIMIZATION Off;

                % disable unweighting
                EVENT_GENERATION_MODE Weighted
                # ANALYSIS Rivet
                }(run);

                (processes){
                # Process 93 93 -> 11 -11;
                Process 93 93 -> 11 -11 93{1};
                CKKW sqr(20/E_CMS);
                Order (*,2);
                Print_Graphs graphs;
                End process;
                }(processes);

                (selector){
                Mass 11 -11 66 116
                }(selector)
                ```

            ```bash title="⏳ 11min"
            # generate new sample
            Sherpa -e 10k EVENT_OUTPUT=HepMC_GenEvent[events_MEPS_UE.hepmc]
            rivet -a MC_ZINC events_MEPS_UE.hepmc.hepmc2g -o Analysis_MEPS_UE.yoda

            # make plots
            rivet-mkhtml --mc-errs Analysis_MEPS.yoda:Title=MEPS Analysis_MEPS_UE.yoda:Title=MEPS_UE --no-weights --output rivet-plots_MEPS_UE
            ```

        After enabling hadronization and underlying events, plots are available [here](https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/standalone/rivet-plots_MEPS_UE/).

### NLO QCD corrections

You can simulate NLO events by adding the following lines to the `Process` block:

```Ini
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
```
Here, you have to replace `LJET` with the final-state multiplicities that you would like
to simulate with NLO QCD corrections, e.g. by using `2` to calculate the `2 -> 2` process
at NLO.
In the MEPS set-up studied above, where up to one additional jet is simulated,
you could even replace `LJET` with `2,3` to calculate both multiplicities at NLO.

Note that the `ME_Generator` is set to Amegic.
This module writes out code the first time it is run for a given process,
which must be compiled.
Therefore, it will exit execution and ask you to run the generated `makelibs` script to compile all process libraries needed. Run it within the container:

=== "📦 Container"

    ```bash title="⏳ 8min"
    # comment "ANALYSIS Rivet" in Run.dat
    Sherpa -e0
    ./makelibs
    Sherpa -e0

    # uncomment "ANALYSIS Rivet" in Run.dat
    Sherpa -e10k -A Analysis_NLO
    ```

=== "🔀 CMSSW"

    ```bash title="⏳ 11min"
    # comment "ANALYSIS Rivet" in Run.dat
    Sherpa -e0
    ./makelibs
    Sherpa -e0
    Sherpa -e 10k EVENT_OUTPUT=HepMC_GenEvent[events_NLO.hepmc]
    rivet -a MC_ZINC events_NLO.hepmc.hepmc2g -o Analysis_NLO.yoda
    ```


You are then ready to run Sherpa again. After the calculation of the total cross section (which will take longer now due to the increased complexity of the NLO matrix elements), you can generate events and compare the new sample to your LO samples using Rivet.

??? tip

    === "📦 Container"

        ```bash title="⏳ 5s"
        # make plots
        rivet-mkhtml --mc-errs Analysis_MEPS_UE.yoda.gz:Title=MEPS_UE Analysis_NLO.yoda.gz:Title=NLO -o rivet-plots_LO_VS_NLO
        ```

    === "🔀 CMSSW"

        ```bash title="⏳ 24s"
        # make plots
        rivet-mkhtml --mc-errs Analysis_MEPS_UE.yoda:Title=MEPS_UE Analysis_NLO.yoda:Title=NLO -o rivet-plots_LO_VS_NLO --no-weights
        ```

    Comparison plots between LO and NLO are available [here](https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/standalone/rivet-plots_LO_VS_NLO/).
