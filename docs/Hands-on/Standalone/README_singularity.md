# Hands-on: Standalone

This tutorial uses the Drell-Yan Z+Jets process as an example.

It is an abridged and modified version of a longer MCnet tutorial given in Durham in 2023:

> Reference: MCnet School 2023 [Sherpa tutorial](https://gitlab.com/hepcedar/mcnet-schools/durham-2023/-/tree/main/sherpa)

## Download container image

We use the container platform `Singularity` (now [`apptainer`](https://apptainer.org/docs/user/latest/)) to pull the Docker image of [`rivet-sherpa`](https://hub.docker.com/r/hepstore/rivet-sherpa).

> If you want to know more about `Singularity`, have a look at the [Basics of Singularity](https://pawseysc.github.io/singularity-containers/12-singularity-intro/index.html).

Run the following commands to set up the image and test if it works correctly
by running the `Sherpa` executable that is installed within the container:

```bash title="⏳ 8min"
singularity pull docker://hepstore/rivet-sherpa:3.1.9-2.2.15
alias container="singularity exec -B $PWD:$PWD ${PWD}/rivet-sherpa_3.1.9-2.2.15.sif"
singularity cache clean
# check if Sherpa available
container Sherpa --version
```

!!! note

    If you are unable to pull the image, you can temporarily use a locally served image.

    ```bash title="⏳ 5s"
    alias container="singularity exec -B $PWD:$PWD /afs/cern.ch/work/j/jixiao/public/sherpa_tutorial/rivet-sherpa_3.1.9-2.2.15.sif"
    # check if Sherpa available
    container Sherpa --version
    ```

## A First Sherpa Run

Many reactions at the LHC suffer from large higher-order QCD corrections. The correct simulation of Bremsstrahlung in these processes is essential. It can be tackled either in the parton-shower approach, or using fixed-order calculations.

Sherpa combines both these methods using a technique known as Matrix Element + Parton Shower merging (ME+PS).[@Buckley:2011ms]
This tutorial will show you how to use the method in Sherpa.

### The Input File

Sherpa is steered using input files (also called runcards), which consist of several sections,
as discussed in the introduction talk.
A comprehensive list of all input parameters for Sherpa is given in the [Sherpa manual](https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.15.html).

The following input file can be used for a simple Z+jets process simulation:

```Ini
(run){
  % collider setup
  BEAM_1 2212; BEAM_ENERGY_1 = 3500.;
  BEAM_2 2212; BEAM_ENERGY_2 = 3500.;

  % disable non-perturbative simulation stages
  MI_HANDLER Off;
  FRAGMENTATION Off;

  % do not require a full optimisation before event generation
  ERROR 0.99;
  FINISH_OPTIMIZATION Off;

  % disable unweighting
  EVENT_GENERATION_MODE Weighted
}(run);

(processes){
  Process 93 93 -> 11 -11;
  Order (*,2);
  Print_Graphs graphs;
  End process;
}(processes);

(selector){
  Mass 11 -11 66 116
}(selector)
```

Since Sherpa writes out data during its runs, it might be advisable to create separate directories for each run.
Run the following commands to create a directory
and download the above runcard.

```bash
mkdir drell-yan
cd drell-yan
cp /afs/cern.ch/work/j/jixiao/public/sherpa_tutorial/standalone/Run.dat .
```

The simulated reactions are defined
in the `(processes)` section of the runcard.
Particles are identified by their PDG codes,
e.g. 11 stands for an electron, while -11 stands for a positron.
Negative codes denote antiparticles.
To specify the initial state we use the special code 93,
which denotes the jet container.
It stands for any parton, i.e. the light quarks and the gluon.

To regularize the cross section, which would diverge if we included the entire photon contribution,
we use a generator-level cut in the `(selector)` section of the runcard.
It ensures that the invariant mass of the dilepton is close to the Z mass.
In this way, we remove the singular behaviour when the invariant mass becomes zero.

### Running Sherpa

Let's now run Sherpa for the first time,
and check if it indeed generates Drell-Yan events.

Run the following command line:
```bash title="⏳ 10s"
container Sherpa -e0
```

When run for the first time, Sherpa will produce diagram information for the calculation of hard scattering processes. It will also compute that hard scattering cross sections, which are stored, together with the diagram information, for subsequent runs.

!!! question

    With the option **-e0** we have instructed Sherpa to produce no event. Have a look at the total cross section. Which one do you find?

Now have a look at the Feynman diagrams, which contribute to the simulation of the hard process:

```bash title="⏳ 30s"
container plot_graphs.sh graphs/
firefox graphs/index.html
```

(The **firefox** command should be run on your host machine, which is why we omit the `container` command in this line. Of course you can use any method to open the HTML file on your host machine.)

!!! question

    Are these the diagrams you expect to find?

??? tip

    Click [here](https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/standalone/graphs_dy/) to see the Feynman diagrams, if you could not open the html.

Note that some diagrams that have identical matrix elements will be omitted,
while flipping the initial-state of different incoming partons
gives rise to separate graphs.

Now run the following command line:

```bash title="⏳ 3s"
container Sherpa -e1 -o3
```

??? info

    Click [here](https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/standalone/standalone_e1_o3.log) to see the log file.

The option **-e1** used above instructs Sherpa to produce one event, and the option **-o3** will print the result of event generation on the screen. You will see Sherpa's internal event record.

!!! question

    Search for **Signal Process** inside the output and check incoming and outgoing particles. Scroll a bit down, and look for the **Shower** stage. How many partons are emitted by the parton shower?


## ME+PS merging

The current runcard lets Sherpa generate events at lowest order in the strong coupling.
To improve the description of real radiative corrections, we can include higher-multiplicity
tree-level contributions in the simulation. This is done by changing the process specification:

```Ini
  Process 93 93 -> 11 -11;
```
to
```Ini
  Process 93 93 -> 11 -11 93{1};
```
The last entry instructs Sherpa to produce up to one additional ``jet'' using
hard matrix elements and combine the respective process with the leading-order process.
This is known as Matrix Element + Parton Shower merging (ME+PS), or the CKKW method.
The essence of the method is a separation of hard from soft radiative corrections,
achieved using phase-space slicing by means of a variable called the jet criterion.
The slicing parameter is called the merging cut.

Let us assume we want to classify jets of more than 20 GeV transverse momentum as hard.
In Sherpa, the corresponding merging cut would be specified as

```Ini
CKKW sqr(20/E_CMS);
```

Therefore, the complete `(processes)` section for the merged event sample reads:

```Ini
(processes){
    Process 93 93 -> 11 -11 93{1};
    CKKW sqr(20/E_CMS);
    Order (*,2);
    Print_Graphs graphs;
    End process;
}(processes);
```

Run the new setup.

```bash title="⏳ 2min"
container Sherpa -e0
```

!!! note

    Sherpa does not only compute the total cross section for the `j j -> e- e+` process, but also for the `j j -> e- e+ j` process with one additional final-state jet.

If you like, you can have a look at the Feynman graphs again, which contribute
to the ME+PS merged event sample. In this case, you also need to rerun the
plot command from the previous section.

## Analyses

By default, Sherpa does not store events. Run Sherpa with the following command
to write out weighted event files in **HepMC3** format, which can subsequently be analyzed

```bash title="⏳ 5s"
container Sherpa -e 1 EVENT_OUTPUT=HepMC3_GenEvent[events.hepmc]
```

Sherpa will produce a file called `events.hepmc`, which can be analysed with Rivet. Alternatively, you can use Sherpa's internal interface to Rivet, to analyse events directly without writing out large amounts of data first. To do that, add the following line to the `(run)` section of your run
card:
```Ini
  ANALYSIS Rivet
```

To tell Rivet what analysis should be used, you have to add a new section to the
run card:

```Ini
(analysis){
  BEGIN_RIVET {
    -a MC_ZINC;
  } END_RIVET;
}(analysis);
```

The option `-a MC_ZINC` instructs Rivet to run a Monte-Carlo analysis of Drell-Yan events, which will provide us with a few observables
that can be used for diagnostic purposes.

You can change the name of the produced yoda file by adding:

```Ini
  ANALYSIS_OUTPUT=Analysis_MEPS
```

in the `(run)` section or alternatively you can specify it via the command line:

```bash title="⏳ 40s"
container Sherpa -e10k -A Analysis_MEPS
```

Here, we have also increase the statistics to 10k events.

You can view the results of this analysis by running the command:

```bash title="⏳ 30s"
container rivet-mkhtml --mc-errs Analysis_MEPS.yoda.gz
```

and opening `rivet-plots/index.html` in a browser.

??? tip

    Click [here](https://jixiao.web.cern.ch/jixiao/gen/sherpa_tutorial_2024/standalone/rivet-plots/) to see histograms after Rivet analysis.

!!! question

    Now it is your turn: Generate a separate file without ME+PS merging (i.e. using the original set-up of the previous section) and analyze it with Rivet. Compare the results of both runs by using `rivet-mkhtml --mc-errs Analysis_LOPS.yoda.gz:Title=LOPS Analysis_MEPS.yoda.gz:Title=MEPS`.

    Do you observe any differences? Why?

## Next steps

If there is still time, feel free to do additional studies.
The sections below suggest a few things to try out, you can go through them in any order.

The first section adds uncertainty estimates to your result, the second enables non-perturbative simulations stages, i.e. Hadronization and Underlying Event, and the last section discusses NLO QCD corrections.

### Uncertainty estimates

Using Sherpa, you can study the renormalization and factorization
scale dependence of the simulation as well as the uncertainty
related to the PDF fit. Thanks to reweighting techniques, this can be done
on-the-fly, without running Sherpa multiple times. The reweighting is enabled in
Sherpa by adding the following flags to the `(run)` section of you input file:

```Ini
  SCALE_VARIATIONS 0.25,0.25 0.25,1 1,0.25 1,4 4,1 4,4;
  PDF_LIBRARY LHAPDFSherpa;
  PDF_SET NNPDF30_nnlo_as_0118;
  PDF_VARIATIONS NNPDF30_nnlo_as_0118[all];
```
The line starting with `SCALE_VARIATIONS` instructs Sherpa to perform a
six-point variation of the renormalization and factorization scales,
with the pairs of numbers representing the scale factors for the squared
renormalization and factorization scales.
The line starting with `PDF_VARIATIONS` instructs Sherpa
to perform a variation using all PDFs in the error set of the `NNPDF30_nnlo_as_0118` PDF set, which
is interfaced through LHAPDF.
Finally, in the middle line the default PDF is set to the nominal PDF of the `NNPDF30_nnlo_as_0118` set.
To make the set available within the container, run the following command:

```bash
container lhapdf install NNPDF30_nnlo_as_0118
```

You will also have to add

```Ini
  HEPMC_USE_NAMED_WEIGHTS 1;
  SKIPWEIGHTS 0;
```

in the `(analysis)` section, after the `BEGIN_RIVET` line.

When you have generated a new sample containing the above variations,
you can include them in the Rivet plot, e.g. by drawing a scale variation band
around the nominal prediction:

```bash
container rivet-mkhtml --mc-errs Analysis.yoda.gz:'BandComponentEnv=MUR.*_MUF.*_PDF261000:Variations=none'
```

With 10k events, the statistical error (given by the vertical bars) should begin to be a bit smaller than
the scale variation (given by the band).

### Hadronization and underlying event

So far, multiple parton interactions, which provide a model for the underlying event,
have not been simulated. Also, hadronization has not been included in order to speed up
event generation. You can enable both by removing or commenting the lines:

```Ini
  MI_HANDLER Off;
  FRAGMENTATION Off;
```

!!! question

    How do the predictions for the observables change and why?

### NLO QCD corrections

You can simulate NLO events by adding the following lines to the `Process` block:

```Ini
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
```
Here, you have to replace `LJET` with the final-state multiplicities that you would like
to simulate with NLO QCD corrections, e.g. by using `2` to calculate the `2 -> 2` process
at NLO.
In the MEPS set-up studied above, where up to one additional jet is simulated,
you could even replace `LJET` with `2,3` to calculate both multiplicities at NLO.

Note that the `ME_Generator` is set to Amegic.
This module writes out code the first time it is run for a given process,
which must be compiled.
Therefore, it will exit execution and ask you to run the generated `makelibs` script to compile all process libraries needed. Run it within the container. Simply run:

```bash
container ./makelibs
```

You are then ready to run Sherpa again. After the calculation of the total cross section (which will take longer now due to the increased complexity of the NLO matrix elements), you can generate events and compare the new sample to your LO samples using Rivet.
